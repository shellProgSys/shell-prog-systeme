#ifndef REMOTE_H
#define REMOTE_H
#include "Shell.h"
#include <string.h>
#include <stdio.h>
#include <unistd.h>
struct machine {
    char nom[20];
    int pipeEntrant[2];
    int pipeSortant[2];
};
struct machine machines[20];

int remote(char **argv);
int commandeArrayRemote(struct machine, char**);
int commandeRemote(struct machine, char*);
int ajouterMachine(char *);
#endif //REMOTE_H