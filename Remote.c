#include "Remote.h"
#include <sys/wait.h>

unsigned short int compteurMachines = 0;

int remote(char **argv) {
    if (LongueurListe(argv) < 2) {
        fprintf(stderr, "Use : remote add/list/remove/all/destName [command]\n");
        return 1;
    }
    
    if (strcmp(argv[1], "add") == 0) { //Ajout de machine
        //printf("Ajout d'une machine", sizeof(argv) / sizeof(char));
        for (int i = 2; i < LongueurListe(argv); i++) {
            printf("Destination adding : %s\n", argv[i]);
            if (ajouterMachine(argv[i]) != 0) {
                return 1;
            }
        }
        return 0;
    }
    
    else if (strcmp(argv[1], "list") == 0) {
        for (int i = 0; i < compteurMachines; i++) {
            printf("#%d - %s\n", i, machines[i].nom);
        }
        return 0;
    }
    
    else if (strcmp(argv[1], "remove") == 0) {
        int i;
        for (i = 0; i < compteurMachines; i++) {
            commandeRemote(machines[i], "exit\n");
        }
        for (i = 0; i < compteurMachines; i++) {
            wait(NULL);
        }
        compteurMachines = 0;
        return 0;
    }
    
    else if (strcmp(argv[1], "all") == 0) {
        for (int i = 0; i < compteurMachines; i++) {
            if (commandeArrayRemote(machines[i], argv+2) != 0) {
                return 1;
            }
        }
        return 0;
    }
    
    else {
        if (LongueurListe(argv) >= 3) {
            for (int i = 0; i < compteurMachines; i++) {
                if (strcmp(machines[i].nom, argv[1]) == 0) { //On cherche la machine demandée
                    return commandeArrayRemote(machines[i], argv+2);
                }
            }
        }
        else {
            printf("Use : remote add/list/remove/all/destName [command]\n");

        }
        return 1;
    }
}

int ajouterMachine(char * machine) {
    strcpy(machines[compteurMachines].nom, machine);

    //Création des pipes

    pipe(machines[compteurMachines].pipeEntrant);
    pipe(machines[compteurMachines].pipeSortant);

    pid_t forkMachine = fork();

    /*
     * pipeSortant[0] = Les résultats des commandes seront lues ici
     * pipeSortant[1] = Les résultats des commandes seront écrits ici
     *
     * pipeEntrant[0] = Les commandes seront lues par SSH ici
     * pipeEntrant[1] = Les commandes seront écrites ici
     */

    if (forkMachine == 0) { //ssh
        close(machines[compteurMachines].pipeEntrant[1]);
        close(machines[compteurMachines].pipeSortant[0]);

        char **ouvertureSSH = InitialiserListeArguments();
        AjouterArg(ouvertureSSH, "ssh");
        AjouterArg(ouvertureSSH, "-T"); //Empèche l'assignation d'un pseudo terminal
        AjouterArg(ouvertureSSH, machine);
        AjouterArg(ouvertureSSH, "\0");

        dup2(machines[compteurMachines].pipeEntrant[0], 0);
        dup2(machines[compteurMachines].pipeSortant[1], 1);
        dup2(machines[compteurMachines].pipeSortant[1], 2);

        execvp("ssh", ouvertureSSH);
        exit(1);
    }
    else {
        close(machines[compteurMachines].pipeEntrant[0]);
        close(machines[compteurMachines].pipeSortant[1]);
        pid_t pipeMachine = fork();
        if (pipeMachine == 0) {
            dup2(machines[compteurMachines].pipeSortant[0], 0);
            execlp("sh", "sh", "xcat.sh", NULL);
            exit(1);
        }
        else {
            compteurMachines++;
            return 0;
        }
    }
    return 1;
}

int commandeArrayRemote(struct machine machine, char** arrayCommande) {
    //Préparation de la commande
    char *commande = malloc(LongueurListe(arrayCommande) * sizeof(char) * 20);
    memset(commande, '\0', sizeof(commande));
    for (int i = 0; i < LongueurListe(arrayCommande); i++) {
        strcat(commande, arrayCommande[i]); //Ajout de la commande morceaux par morceaux
        strcat(commande, " "); //Ajout de la commande morceaux par morceaux
    }
    strcat(commande, "\n"); //Ajout du NULL qui marque la fin de la commande
    int statusCommande = commandeRemote(machine, commande);
    free(commande);
    return statusCommande;
}

int commandeRemote(struct machine machine, char* commande) {
    return (write(machine.pipeEntrant[1], commande, strlen(commande)) >= 0 ? 0 : 1); //On l'envoi au processus SSH
}