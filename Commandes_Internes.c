#include "Commandes_Internes.h"
#include <readline/history.h>
#include <time.h>
int echo(char ** argv)
{
    int i, longueur = LongueurListe(argv);
    for (i=1;i<longueur;i++)
    {
        printf("%s",argv[i]);
        if (i != (longueur + 1)) {
            printf(" ");
        }
    }
    printf("\n");
    return 0;
}

int date(char ** argv)
{
    time_t t = time(NULL);
    struct tm tm = *localtime(&t);
    printf("%s",asctime(&tm));
    return 0;
}

int cd(char ** argv)
{
    return chdir(argv[1]);
}

int pwd(char **argv)
{
    char cwd[1024];
    printf("%s\n",getcwd(cwd,1024));
    return 0;
}

int history(char **argv)
{
    HIST_ENTRY **the_history_list;
    the_history_list=history_list();
    int i;
    for (i=0;i<history_length;i++)
    {
        printf("%s\n",the_history_list[i]->line);
    }
    return 0;
}

int hostname(char **argv)
{
    char name[1024];
    gethostname(name,1024);
    printf("%s\n",name);
    return 0;
}
