#include "Shell.h"
#include "Evaluation.h"
#include "Commandes_Internes.h"
#include "Remote.h"

#include <sys/wait.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <fcntl.h>

int evaluer_expr(Expression *e) {
    int statusSequence;
    int p[2];
    int pid;
    int statusExpr = 1; //Status finale de l'evaluation (ce qui sera retourné par la fonction)
    switch (e->type)
    {
        //**********Commandes Internes**********//
        case SIMPLE:
            // ECHO
            if (strcmp(e->arguments[0],"echo") == 0)
            {
                statusExpr = echo(e->arguments);
            }

            // DATE
            else if (strcmp(e->arguments[0],"date") == 0)
            {
                statusExpr = date(e->arguments);
            }

            // CD
            else if (strcmp(e->arguments[0],"cd") == 0)
            {
                statusExpr = cd(e->arguments);
            }

            // PWD
            else if (strcmp(e->arguments[0],"pwd") == 0)
            {
                statusExpr = pwd(e->arguments);
            }

            // HISTORY
            else if (strcmp(e->arguments[0],"history") == 0)
            {
                statusExpr = history(e->arguments);
            }

            // HOSTNAME
            else if (strcmp(e->arguments[0],"hostname") == 0)
            {
                statusExpr = hostname(e->arguments);
            }

            // KILL
            else if (strcmp(e->arguments[0],"kill") == 0)
            {
                statusExpr = kill(atoi(e->arguments[1]),atoi(e->arguments[2]));
            }

            // EXIT
            else if (strcmp(e->arguments[0],"exit") == 0)
            {
                char **commandeRemoteExit = InitialiserListeArguments();
                AjouterArg(commandeRemoteExit, "remote");
                AjouterArg(commandeRemoteExit, "remove");
                remote(commandeRemoteExit);
                exit(0);
            }

            //**********Remote shell**********//
            else if (strcmp(e->arguments[0],"remote") == 0)
            {
                statusExpr = remote(e->arguments);
            }

            //**********Commandes Externes**********//
            else
            {
                pid = fork();
                if (pid==0)
                {
                    execvp(e->arguments[0],e->arguments);
                    exit(1);
                }
                else
                {
                    wait(&statusExpr);
                    statusExpr = WEXITSTATUS(statusExpr);
                }
            }
            break;

        //**********Exressions**********//
        // expression &
        case BG:
            pid = fork();
            if (pid==0)
            {
                execvp(e->arguments[0],e->arguments);
                return 1;
            }
            else {
                statusExpr = 0;
            }
            break;

        // expression ; expression
        case SEQUENCE:
            evaluer_expr(e->gauche);
            statusExpr = evaluer_expr(e->droite);
            break;

        // expression || expression
        case SEQUENCE_OU:
            statusExpr = evaluer_expr(e->gauche);
            if (statusExpr != 0)
                statusExpr = evaluer_expr(e->droite);
            break;

        // expression && expression
        case SEQUENCE_ET:
            statusExpr = evaluer_expr(e->gauche);
            if (statusSequence == 0)
                statusExpr = evaluer_expr(e->droite);
            break;

        // (expression)
        case SOUS_SHELL:
            statusExpr = evaluer_expr(e->gauche);
            break;

        // expression | expression
        case PIPE:
            pid = fork();
            if (pid==0)
            {
              int p[2];
              pipe(p);
              int pid2 = fork();
              if (pid2 == 0)
              {
                close(p[0]);
                dup2(p[1], 1);
                close(p[1]);
                exit(evaluer_expr(e->gauche));
              }
              else
              {
                close(p[1]);
                dup2(p[0],0);
                close(p[0]);
                int statusDroite = evaluer_expr(e->droite);
                wait(NULL);
                exit(statusDroite);
              }
            }
            else {
              waitpid((int) pid,&statusExpr,0);
              statusExpr = WEXITSTATUS(statusExpr);
            }
            break;
        // expression > fichier
        case REDIRECTION_O:
            pid=fork();
            if (pid==0)
            {
                int fd = open(e->arguments[0], O_WRONLY | O_CREAT, 0666);
                dup2(fd,1);
                execvp(e->gauche->arguments[0],e->gauche->arguments);
                close(fd);
                exit(1);
            }
            else
            {
                wait(&statusExpr);
                statusExpr = WEXITSTATUS(statusExpr);
            }
            break;

        // expression 2> fichier
        case REDIRECTION_E:
            pid=fork();
            if (pid==0)
            {
                int fd = open(e->arguments[0], O_WRONLY | O_CREAT, 0666);
                dup2(fd,2);
                execvp(e->gauche->arguments[0],e->gauche->arguments);
                close(fd);
                exit(1);
            }
            else
            {
                wait(&statusExpr);
                statusExpr = WEXITSTATUS(statusExpr);
            }
            break;

        // expression &> fichier
        case REDIRECTION_EO:
            pid=fork();
            if (pid==0)
            {
                int fd = open(e->arguments[0], O_WRONLY | O_CREAT, 0666);
                dup2(fd,1);
                dup2(fd,2);
                execvp(e->gauche->arguments[0],e->gauche->arguments);
                close(fd);
                exit(1);
            }
            else
            {
                wait(&statusExpr);
                statusExpr = WEXITSTATUS(statusExpr);
            }
            break;

        // expression < fichier
        case REDIRECTION_I:
            pid=fork();
            if (pid==0)
            {
                printf("%s",e->arguments[0]);
                int fd = open(e->arguments[0], O_RDONLY);
                dup2(fd,0);
                execvp(e->gauche->arguments[0],e->gauche->arguments);
                close(fd);
                exit(1);
            }
            else
            {
                wait(&statusExpr);
                statusExpr = WEXITSTATUS(statusExpr);
            }
            break;

        // expression >> fichier
        case REDIRECTION_A:
            pid=fork();
            if (pid==0)
            {
                int fd = open(e->arguments[0], O_WRONLY | O_CREAT, 0666);
                lseek(fd,0, SEEK_END);
                dup2(fd,1);
                execvp(e->gauche->arguments[0],e->gauche->arguments);
                close(fd);
                exit(1);
            }
            else
            {
                wait(&statusExpr);
                statusExpr = WEXITSTATUS(statusExpr);
            }
            break;
        // Sinon...
        default:
            break;
    }
    return statusExpr;
}
