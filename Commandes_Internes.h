#ifndef COMINTERN_H
#define COMINTERN_H

#include "Shell.h"

extern int echo(char ** argv);
extern int date(char ** argv);
extern int cd(char ** argv);
extern int pwd(char ** argv);
extern int history(char ** argv);
extern int hostname(char ** argv);
#endif
